const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({

	firstName:{
		type: String,
		required: [true,'First name is required.']
	},
	lastName:{
		type: String,
		required: [true,'Last name is required.']
	},
	birthday:{
		type: Date,
		required: [true,'Birthday is required.']
	},
	address:{
		type: String,
		required: [true,'Address is required.']
	},
	isAdmin:{
		type: Boolean,
		default: false 
	},
	email:{
		type: String,
		required: [true,'Email is required.']
	},
	password:{
		type: String,
		required: [true,'Password is required.']
	},
	mobileNo:{
		type: String,
		required: [true,'Mobile number is required.']
	},

	userOrders:[
			{
				productId:{
					type: String,
					required: [true, "ProductId is required"]
				},

				quantity:{
					type: Number,
					required: [true, "Quantity is required"]
				},

				subtotal:{
					type: Number,
					required: [true, "subtotal is required"]
				}
			}
		],

	
	totalAmount:[{
		type: Number,
		default: 0
		}],

	PurchasedOn:{
		type: Number,
		default:  new Date()
		}
		
})

module.exports = mongoose.model('User', userSchema);