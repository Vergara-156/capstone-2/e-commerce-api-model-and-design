const mongoose = require('mongoose')

const productSchema = new mongoose.Schema({

	name:{
		type: String,
		required:  [true, "Product Name is Required"]

	},
	type: {
		type: String,
		required: [true, "Product Type is Required"]
	},

	description:{
		type: String,
		required:  [true, "Product Description is required"]

	},
	price:{
		type: Number,
		required:  [true, "Product Price is required"]

	},
	isActive:{
		type: Boolean,
		default: true
	},
	productcreatedOn:{
		type: Date,
		default: new Date()

	},
	productOrders:[
			{

			userId:{
				type: String,
				required: [true, "UserId is required"]
			},

			Quantity:{
				type: Number,
				required: [true, "Quantity is required"]
			}
		}
	],

	stock:{
		type: Number,
		required: [true, "Quantity is required"]

	}

});

module.exports = mongoose.model('Product',productSchema)