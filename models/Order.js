const mongoose = require('mongoose')

const orderSchema = new mongoose.Schema({

	productOrder:{
		type: String,
		required: [true,'Name of Product is required']
	},
	orderQuantity:{
		type: Number,
		required: [true,'Order Quantity is required']
	},

	// countryRegion:{
	// 	type: String,
	// 	required: [true,'is required']
	// },
	// modeOfPayment:{
	// 	type: String,
	// 	required: [true,'is required']
	// },
})

module.exports = mongoose.model('Order',orderSchema)