const dotenv =require('dotenv')
const auth = require('./auth')
dotenv.config()
const jwt = require('jsonwebtoken')
console.log(process.env.SECRET)

let secret = process.env.SECRET

module.exports.createAccessToken = (authUser) =>{
	
	let userData ={
		id: authUser._id,
		email: authUser.email,
		isAdmin: authUser.isAdmin
	};
	return jwt.sign(userData,secret,{})


};

module.exports.verify= (req,res,next)=>{
	let token = req.headers.authorization;
	
	console.log(typeof token)
	
if (typeof token !== 'undefined') {
	console.log('token is Valid')
	
	token = token.slice(7, token.length)
	jwt.verify(token,secret,(err,payload)=>{
		if (err) {
			return res.send({auth: 'Auth Failed'})
		} else {
			next();
		}
	})
} else {
	return res.send({auth: "Authorization Failed, Check token"})
}
}

module.exports.decode = (accessToken,payload) => {
	if (typeof accessToken !== 'undefined') {
		accessToken = accessToken.slice(7, accessToken.length)
		return jwt.verify(accessToken,secret,(err,data)=>{
				if (err) {
				return null
			} else {
				return jwt.decode(accessToken,{complete: true}).payload

			}
		})
			
	}
};
