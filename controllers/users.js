const User = require ('../models/User')
const bcrypt = require('bcrypt')
const auth = require('../auth')

module.exports.registerUser = (client) => {
	let fName = client.firstName
	let lName = client.lastName
	let cEmail = client.email
	let cPassword = client.password
	let cAddress = client.address
	let cBirthday = client.birthday
	let cMobileno = client.mobileNo
	
	let newUser= new User({
		firstName: fName,
		lastName: lName,
		email: cEmail,
		password: bcrypt.hashSync(cPassword,10),
		address: cAddress,
		birthday: cBirthday,
		mobileNo: cMobileno

	})

	return newUser.save().then((saved,err) =>{
		
		if (saved) {
			return saved
		} else {
			return false
		}
		
	})
}

module.exports.getAllUsers =()=>{

	return User.find({}).then(result =>{
	return result;

 })
}
module.exports.getProfile = (id)=>{
	
	return User.findById(id).then(result =>{
		return result;
	})
}

module.exports.setAsAdmin=(user) =>{
	let id = user.userId
	let updates ={
		isAdmin: true
	}
	return User.findByIdAndUpdate(id,updates).then((admin,err)=>{
			if (admin) {
				return true;
			} else {
				return 'Updates Failed to Implement'
			}
	})
	
}
module.exports.setAsNonAdmin =(user)=>{
	let id = user.userId
	let updates = {
		isAdmin: false
	}
	return User.findByIdAndUpdate(id,updates).then((user,err)=>{
			if (user) {
				return false
			} else {
				'Updates Failed to Implement'
			}
	})
}

module.exports.loginUser = (reqBody) => {
     return User.findOne({email: reqBody.email}).then(result => {
        let uPassW = reqBody.password;
        if (result === null) {
          return 'Email Does Not Exist';
        } else {
           const isMatched = bcrypt.compareSync(uPassW, result.password); 
           if (isMatched) {
             let userdata= result.toObject();
             return {accessToken: auth.createAccessToken(userdata)};
           } else {
             return 'Passwords Does Not Match. Check Credentials!'; 
           }
        };
     });
  };