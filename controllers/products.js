const Product = require('../models/Product')

module.exports.createProduct = (data) => {

	let info = data.product;
	let ProdName = info.name;
	let ProdType = info.type;
	let ProdDesc = info.description;
	let ProdPrice = info.price;
	let ProdStock = info.stock;
	
	let newProduct = new Product ({
		name: ProdName,
		type: ProdType,
		description: ProdDesc,
		price: ProdPrice,
		stock: ProdStock,
			
	})

			
	return newProduct.save().then((savedProduct,err)=>{
		if (savedProduct) {
			return `Successfully saved ${ProdName} with quantity of ${ProdPrice} pesos`
		} else {
			return `Product Registration Failed, Try Again`
		}	

	})


}

module.exports.getAllProducts =()=>{

	return Product.find({}).then(searchResult =>{
	return searchResult;

 })
}

module.exports.getAllActive =()=>{
return Product.find({isActive:true}).then(result =>{
	return result;
})
}

module.exports.getProduct = (data) =>{
	
	return Product.findById(data).then(result =>{
		return result;
	})

}



module.exports.updateProductInfo = (product,details) =>{
	
	let ProdName = details.name;
	let ProdType = details.type;
	let ProdDesc = details.description;
	let ProdPrice = details.price;
	let ProdStock = details.stock;
	
	let updateProduct = {

		productName: ProdName,
		productType: ProdType,
		productDescription: ProdDesc,
		productPrice: ProdPrice,
		productStock: ProdStock
	}

	let pId = product.update

	return Product.findByIdAndUpdate(pId,updateProduct).then((result,err)=>{

		if (result) {
			return result
		} else {
			return 'Update Failed. Try Again'
		}

	})


}

module.exports.archiveProduct =(product)=>{
	
	let updates = {
		isActive: false
	}
	return Product.findByIdAndUpdate(product,updates).then((archived,err)=>{
		if (archived) {
			return 'Course archived'
		} else {
			return false
		}

	})
}