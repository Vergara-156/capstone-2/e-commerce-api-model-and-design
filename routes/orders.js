const exp = require('express')
const controller = require('../controllers/orders')
const auth = require('../auth')
const route = exp.Router()

route.post('/create-order', auth.verify , (req,res)=>{

	let token =req.headers.authorization
	let payload = auth.decode(token)
	let isAdmin = payload.isAdmin
	let userId = payload.id 
	let productsOrderId = req.body.productId
	let quantityOrder = req.body.quantity

		let data = {
			userId :userId,
			productId : productsOrderId,
			quantity : quantityOrder,
			
			
	}
		if(!isAdmin) {
		controller.createOrder(data).then(outcome => {
 		res.send(outcome)
 	})
	} else {
		res.send('User is Admin')
	}
	
})

route.get('/all',auth.verify,(req,res) =>{
	let token = req.headers.authorization;
	let payload = auth.decode(token)
	let isAdmin = payload.isAdmin
	isAdmin ? controller.getAllOrders().then(result => 
		res.send(result)) 
	: res.send('Unauthorized User')
	

})

route.get('/details', auth.verify ,(req, res) => {
	let token =req.headers.authorization
	let payload = auth.decode(token)
	let isAdmin = payload.isAdmin
	let userId = payload.id 
	!isAdmin ? controller.getProfile(userId).then(outcome => 
			res.send(outcome))
		: res.send('Unauthorized User')
		
	});

module.exports = route;