const exp = require('express')
const controller = require('../controllers/users')
const auth = require('../auth')
const route = exp.Router()

route.post('/register',(req,res)=>{
	let userData = req.body
	controller.registerUser(userData).then(outcome =>{
		res.send(outcome);
	})
});

route.get('/all',(req,res) =>{
	controller.getAllUsers().then(result => {
		res.send(result);
	})

})
route.get('/details', auth.verify ,(req, res) => {
		let userData = auth.decode(req.headers.authorization);
		let userId = userData.id;
		controller.getProfile(userId).then(outcome => {
			res.send(outcome);
		});
	});

route.put('/:userId/set-as-admin',auth.verify,(req,res)=>{
	let token =req.headers.authorization
	let isAdmin = auth.decode(token).isAdmin
	let id = req.params;
	(isAdmin) ?  controller.setAsAdmin(id).then(outcome =>
			res.send(outcome))
			: res.send('Unauthorized User')
	})


route.put('/:userId/set-as-user',auth.verify,(req,res)=>{
	let token =req.headers.authorization
	let isAdmin = auth.decode(token).isAdmin
	let id = req.params;
	(isAdmin) ?  controller.setAsNonAdmin(id).then(outcome =>
			res.send(outcome))
			: res.send('Unauthorized User')
	})

route.post('/login', (req, res) => {
		let data = req.body;
		controller.loginUser(data).then(outcome => {
			res.send(outcome);
		});
	});

module.exports = route;