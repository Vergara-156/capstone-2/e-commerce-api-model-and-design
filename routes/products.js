const exp = require('express')
const controller = require('../controllers/products')
const auth = require('../auth')
const route =exp.Router()

route.post('/register-product', auth.verify, (req,res)=>{
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	let data ={
		product: req.body
	};
	if(isAdmin) {
		controller.createProduct(data).then(outcome => {
 		res.send(outcome)
 	})
	} else {
		res.send('User is not Authorized')
	}
	
})

route.get('/all',auth.verify,(req,res) =>{
	let token = req.headers.authorization;
	let payload = auth.decode(token)
	let isAdmin = payload.isAdmin
	isAdmin ? controller.getAllProducts().then(result => 
		res.send(result)) 
	: res.send('Unauthorized User')
	

})
route.get('/active',(req,res)=>{
	controller.getAllActive().then(result =>{
		res.send(result)
	})

})

route.get('/:id',(req,res)=>{
	let data = req.params.id
	controller.getProduct(data).then(result =>{
		res.send(result)
	})
})



route.put('/:update',auth.verify,(req,res)=>{
	let params = req.params
	let body = req.body
	isAdmin = auth.decode(req.headers.authorization).isAdmin
	if (isAdmin) {

	controller.updateProductInfo(params,body).then(result =>{
		res.send(result)
	})
	} else {
		res.send('User not Authorized')
	}
	
})

route.put('/:update/archive',auth.verify,(req,res)=>{
	let token =req.headers.authorization
	let isAdmin = auth.decode(token).isAdmin
	let params = req.params.update;
		(isAdmin) ? 
			controller.archiveProduct(params).then(result=>{
			res.send(result);
	})
			: res.send('Unauthorized User')
})


module.exports = route;
